#include <querycpp/querycpp.hpp>

#include <pqxx/pqxx>
#include <fmt/core.h>

#include <string>
#include <vector>

#include <cstdlib>

const std::string tbl_name = "test_entries";

const std::string id_col = "id";
const std::string name_col = "name";
const std::string age_col = "age";

struct test_entry
{
    size_t id;
    std::string name;
    int age;

    test_entry(size_t idx, std::string namex, int agex) : id(idx), name(namex), age(agex){}
    test_entry(std::string namex, int agex) : id(0), name(namex), age(agex){}     
};

bool get_secret_connection_info(std::string& username, std::string& password, std::string& database)
{

    bool all_found = true;
    
    if(const char* env_p = std::getenv("QUERRY_CPP_TEST_DB"))
    {
        database = std::string(env_p);
    }
    else
    {
        std::cout << "Env var for: QUERRY_CPP_TEST_DB not found\n";
        all_found = false;
    }

    if(const char* env_p = std::getenv("QUERRY_CPP_TEST_DB_PASS"))
    {
        password = std::string(env_p);
    }
    else
    {
        std::cout << "Env var for: QUERRY_CPP_TEST_DB_PASS not found\n";
        all_found = false;
    }

    if(const char* env_p = std::getenv("QUERRY_CPP_TEST_DB_USER"))
    {
        username = std::string(env_p);
    }
    else
    {
        std::cout << "Env var for: QUERRY_CPP_TEST_DB_USER not found\n";
        all_found = false;
    }        

    return all_found;
}

bool is_same_entry(const test_entry& lhs, const test_entry& rhs)
{
    return lhs.id == rhs.id && lhs.name == rhs.name && lhs.age == rhs.age;
}

std::vector<struct test_entry> get_test_entries()
{

//    auto postgres_entry = ;
    
    std::vector<struct test_entry> data
    {
        test_entry("postgres", 26),
        test_entry("mysql", 27),
        test_entry("oracle", 44)
     };

    return data;
}

void insert_test_entries(std::vector<struct test_entry>& entries, pqxx::connection& con, const querycpp::table& tbl,
                         const querycpp::column& col_name, const querycpp::column& col_age, const querycpp::column& col_id)
{
    std::vector<std::vector<std::string>> values;

    for (const auto& entry : entries)
    {
        values.push_back({fmt::format("'{}'", entry.name), std::to_string(entry.age)});
    }



    pqxx::work worker{con};
        
    querycpp::query query(tbl);
    query.INSERT({col_name, col_age}, values).returning(col_id);

    std::vector<size_t> ids; 
    for (auto const& [id] : worker.stream<long>(query.str()))
    {

        ids.push_back(id);
    }

    worker.commit();
    
    assert(ids.size() == entries.size());

    for (size_t i = 0; i < entries.size(); ++i)
    {
        entries.at(i).id = (size_t) ids.at(i);
    }
}

std::vector<test_entry> get_all_entries(pqxx::connection& con, const querycpp::table& tbl,
                                        const querycpp::column& col_id, const querycpp::column& col_name, const querycpp::column& col_age)
{
    std::vector<test_entry> entries;

    querycpp::query query{tbl};
    query.SELECT(col_id, col_name, col_age);

    pqxx::work worker{con};
    for (auto const& [id, name, age] : worker.stream<long, std::string, int>(query.str()))
    {
        entries.push_back(test_entry(id, name, age));
    }
    
    return entries; 
}

int main(void)
{

    querycpp::column id(id_col, querycpp::type::postgres::numerical::SERIAL, {querycpp::constraints::PRIMARY});
    querycpp::column name(name_col, querycpp::type::common::string::VARCHAR, {"255"});
    querycpp::column age(age_col, querycpp::type::postgres::numerical::NUMERIC);
    querycpp::table tbl(tbl_name, {id, name, age});    
    

    std::string host = "localhost"; 
    std::string port = "5432";
    std::string username;
    std::string password; 
    std::string dbname;

    assert(get_secret_connection_info(username, password, dbname));
    
    
    pqxx::connection con(fmt::format("host={} port={} user={} password={} dbname={}", host, port, username, password, dbname));
    
    auto entries = get_test_entries();
    insert_test_entries(entries, con, tbl, name, age, id);
    
    auto selected_entries = get_all_entries(con, tbl, id, name, age);

    assert (entries.size() == selected_entries.size());

    auto entry_it = entries.begin();
    auto selected_it = selected_entries.begin();

    while( entry_it != entries.end() && selected_it != selected_entries.end())
    {
        assert(is_same_entry(*entry_it, *selected_it));
        
        entry_it = entry_it + 1;
        selected_it = selected_it + 1;
    }

    querycpp::query query(tbl);
    query.DELETE();
    pqxx::work worker{con};
    worker.exec(query.str());
    worker.commit();
}
