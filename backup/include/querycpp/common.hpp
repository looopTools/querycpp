#ifndef QUERYCPP_COMMON_HPP
#define QUERYCPP_COMMON_HPP

#include <string>

namespace querycpp::common
{
namespace symbols
{
    const std::string left_parenthese  = "(";
    const std::string right_parenthese = ")";
    const std::string wildecard = "*";
    const std::string quote = "'";
}    
}

#endif /*QUERYCPP_COMMON_HPP*/ 
