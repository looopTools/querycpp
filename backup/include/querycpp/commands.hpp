#ifndef QUERYCPP_COMMANDS_HPP
#define QUERYCPP_COMMANDS_HPP

#include <string>

namespace querycpp::commands
{
    
    const std::string table = "TABLE"; 

    // CUD
    const std::string create = "CREATE";
    const std::string update = "UPDATE";
    const std::string drop = "DROP";
    const std::string remove = "DELETE";

    const std::string from = "FROM"; 

    // CREATE CONSTRAITNS
    const std::string if_not_exists = "IF NOT EXISTS"; 
    
    // General
    const std::string where = "WHERE";

    const std::string AND = "AND";
    const std::string OR = "OR"; 

    /// SELECT 
    const std::string select = "SELECT";

    /// LIST Operators
    const std::string in = "IN";

    /// SET
    const std::string set = "SET";

    /// ORDER
    const std::string order_by = "ORDER BY";
    const std::string group_by = "GROUP BY";
    const std::string ascending = "ASC";
    const std::string descending = "DSC";

    /// COUNT and EXISTS
    const std::string count = "COUNT";
    const std::string exists = "EXISTS";

    /// INSERT RELEVANT
    const std::string insert = "INSERT";
    const std::string into = "INTO";
    const std::string values = "VALUES";
    const std::string returning = "RETURNING";

    const std::string references = "REFERENCES";
        
    
}

#endif /* QUERYCPP_COMMANDS_HPP */
