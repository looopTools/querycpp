#ifndef QUERYCPP_DATATYPES_CHARX_HPP
#define QUERYCPP_DATATYPES_CHARX_HPP

#include <cstddef>
#include <format>


#include <querycpp/datatypes/types.hpp>

namespace querycpp::datatypes
{
class charx
{

public:

    charx(size_t size) : _size(size) {}

    std::string string() const
    {
        return _size == 0 ? std::format("{}()", _char) : std::format("{}({})", _char, _size);
    }
private:
    size_t _size;
};
}
#endif /*QUERYCPP_DATATYPES_CHARX_HPP*/
