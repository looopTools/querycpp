#ifndef QUERYCPP_DATATYPES_BPCHAR_HPP
#define QUERYCPP_DATATYPES_BPCHAR_HPP

#include <cstddef>
#include <format>


#include <querycpp/datatypes/types.hpp>

namespace querycpp::datatypes
{
class bpchar
{

public:

    bpchar(size_t size) : _size(size) {}

    std::string string() const
    {
        return _size == 0 ? std::format("{}()", _bpchar) : std::format("{}({})", _bpchar, _size);
    }
private:
    size_t _size;
};
}
#endif /*QUERYCPP_DATATYPES_BPCHAR_HPP*/
