#pragma once

#include <concepts>
#include <string>

#include <fmt/core.h>

namespace querycpp::helpers
{

    std::string escape_string(const std::string& str)
    {
        return fmt::format("'{}'", str);
    }

    template<typename T> concept has_string_function = requires(T t) { t.string(); };

    template<typename T> concept has_name_function = requires(T t) { t.name(); };    
    
}    
