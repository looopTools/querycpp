#pragma once

#include <string>

#include <fmt/core.h>

#include <querycpp/helpers.hpp>

namespace querycpp
{

template <helpers::has_string_function SqlDataType> class column
{

public:

    column(const std::string& name, const SqlDataType& data_type) : _name(name), _data_type(data_type) {}
    std::string name() const { return _name; }

    SqlDataType data_type() const { return _data_type; }

    std::string full_string() { return fmt::format("{} {}", _name, _data_type.string());}
    
private:
    std::string _name;
    SqlDataType _data_type;
};
}
