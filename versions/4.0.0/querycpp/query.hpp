#pragma once

#include "commands.hpp"
#include <querycpp/commands.hpp>
#include <querycpp/operators.hpp>

#include <fmt/core.h>

#include <string>
#include <type_traits>



namespace querycpp
{
class query
{
public:
    query() {}

    template<typename T> query& q_create(T table)
    {
        auto command = fmt::format("{} {} {}", commands::_create, commands::_table, entry_to_string(table));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1, typename... T2> query& q_create(T1 table, T2... columns) {
        auto command = fmt::format("{} {} {} ({})", commands::_create, commands::_table, entry_to_string(table), expand_to_comma_seperated_list(columns...));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T> query& q_drop(T table) {
        auto command = fmt::format("{} {} {}", commands::_drop, commands::_table, entry_to_string(table));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1, typename... T2> query& q_insert_into(T1 table, T2... columns) {
        auto command = fmt::format("{} {} {} ({})", commands::_insert, commands::_into, entry_to_string(table), expand_to_comma_seperated_list(columns...));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_values() {
        _query = _query.empty() ? commands::_values : fmt::format("{} {}", _query, commands::_values);
        return *this;
    }

    template<typename... T> query& q_values(T... values) {
        _query = _query.empty() ? commands::_values : fmt::format("{} {}", _query, commands::_values);

        add_values(values...);
        return *this;
    }

    query& q_select()
    {
        _query = _query.empty() ? commands::_select : fmt::format("{} {}", _query, commands::_select);

        return *this;
    }

    template<typename... T> query& q_select(T... columns)
    {
        auto command = fmt::format("{} {}", commands::_select, expand_to_comma_seperated_list(columns...));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_select_all()
    {
        auto command = fmt::format("{} {}", commands::_select, operators::ALL);
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);

        return *this;
    }

    query& q_select_top(const uint32_t count)
    {
        auto command = fmt::format("{} {} {}", commands::_select, commands::_top, count);
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);

        return *this;
    }

    template<typename T> query& q_from(T table)
    {
        auto command = fmt::format("{} {}", commands::_from, entry_to_string(table));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);

        // TODO: Add support for custom table type
        return *this;
    }

    query& q_where()
    {
        _query = _query.empty() ? commands::_where : fmt::format("{} {}", _query, commands::_where);
        return *this;
    }

    template<typename T> query& q_where(T col)
    {
        auto command = fmt::format("{} {}", commands::_where, entry_to_string(col));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_and() {
        _query = _query.empty() ? commands::_where : fmt::format("{} {}", _query, commands::_and);
        return *this;
    }

    template<typename T> query& q_and(T col)
    {
        auto command = fmt::format("{} {}", commands::_and, entry_to_string(col));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_or() {
        _query = _query.empty() ? commands::_where : fmt::format("{} {}", _query, commands::_or);
        return *this;
    }

    template<typename T> query& q_or(T col)
    {
        auto command = fmt::format("{} {}", commands::_or, entry_to_string(col));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_in()
    {
        _query = _query.empty() ? commands::_in : fmt::format("{} {}", _query, commands::_in);
        return *this;
    }

    query& q_not_in()
    {
        _query = _query.empty() ? commands::_not  + " " + commands::_in : fmt::format("{} {} {}", _query, commands::_not, commands::_in);
        return *this;
    }

    template<typename T> query& q_in(T rhs)
    {
        auto command = fmt::format("{} {}", commands::_in, entry_to_string(rhs));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename... T> query& q_in(T... entries)
    {
        auto command = fmt::format("{} ({})", commands::_in, expand_to_comma_seperated_list(entries...));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1, typename T2> query& q_lt(T1 lhs, T2 rhs)
    {
        return q_less_than(lhs, rhs);
    }

    template<typename T1, typename T2> query& q_less_than(T1 lhs, T2 rhs)
    {
        auto command = fmt::format("{} {} {}", entry_to_string(lhs), commands::_less_than, entry_to_string(rhs));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);

        return *this;
    }

    template<typename T1, typename T2> query& q_gt(T1 lhs, T2 rhs)
    {
        return q_greater_than(lhs, rhs);
    }

    template<typename T1, typename T2> query& q_greater_than(T1 lhs, T2 rhs)
    {
        auto command = fmt::format("{} {} {}", entry_to_string(lhs), commands::_greater_than, entry_to_string(rhs));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1, typename T2> query& q_equal(T1 lhs, T2 rhs)
    {
        auto command = fmt::format("{} {} {}", entry_to_string(lhs), commands::_equal, entry_to_string(rhs));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }


    template<typename T1, typename T2> query& q_like(T1 lhs, T2 rhs)
    {
        auto command = fmt::format("{} {} {}", entry_to_string(lhs), commands::_like, entry_to_string(rhs));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_inner_join(T1 t)
    {
        std::string command = fmt::format("{} {} {}", commands::_inner, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_outer_join(T1 t)
    {
        std::string command = fmt::format("{} {} {}", commands::_outer, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_left_outer_join(T1 t)
    {
        std::string command = fmt::format("{} {} {} {}", commands::_left, commands::_outer, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_right_outer_join(T1 t)
    {
        std::string command = fmt::format("{} {} {} {}", commands::_right, commands::_outer, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_full_outer_join(T1 t)
    {
        std::string command = fmt::format("{} {} {} {}", commands::_full, commands::_outer, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_cross_join(T1 t)
    {
        std::string command = fmt::format("{} {} {}", commands::_cross, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_cross_apply(T1 t)
    {
        std::string command = fmt::format("{} {} {}", commands::_cross, commands::_apply, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    template<typename T1> query& q_outer_apply(T1 t)
    {
        std::string command = fmt::format("{} {} {}", commands::_outer, commands::_join, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_on()
    {
        _query = _query.empty() ? commands::_on : fmt::format("{} {}", _query, commands::_on);
        return *this;
    }

    template<typename T1> query& q_as(T1 t)
    {
        std::string command = fmt::format("{} {}", commands::_as, entry_to_string(t));
        _query = _query.empty() ? command : fmt::format("{} {}", _query, command);
        return *this;
    }

    query& q_right_parenthese()
    {
        _query = _query.empty() ? ")" : fmt::format("{})", _query);
        return *this;
    }

    query& q_left_parenthese()
    {
        _query = _query.empty() ? "(" : fmt::format("({}", _query);
        return *this;
    }

    query& q_nest()
    {
        _query = _query.empty() ? "()" : fmt::format("({})", _query);
        return *this;
    }

    std::string string() const
    {
        return _query;
    }

    query& clear()
    {
        _query.clear();
        return *this;
    }

private:

    template<typename T1, typename... T2> void add_values(T1 value, T2... values)
    {
        _query = fmt::format("{} ({}),", _query, value);
        add_values(values...);
    }

    template<typename T> void add_values(T value)
    {
        _query = fmt::format("{} ({})", _query, value);
    }

    template<typename T1, typename... T2> std::string expand_to_comma_seperated_list(T1 entry, T2... entries)
    {
        std::string result = fmt::format("{}, {}", entry_to_string(entry), expand_to_comma_seperated_list(entries...));
        return result.at(result.length() - 1) == ' ' ? result.substr(0, result.length() - 2) : result; // Todo always remove last ,
    }


    template<typename T> std::string expand_to_comma_seperated_list(T entry)
    {
        return entry_to_string(entry);
    }

    template<typename T> std::string entry_to_string(T entry)
    {
        if constexpr (std::is_same<T, const char *>::value)
        {
            return entry;
        }
        else if constexpr (std::is_same<T, char *>::value)
        {
            return entry;
        }
        else if constexpr (std::is_same<T, std::string>::value)
        {
            return entry;
        }
        else if constexpr (std::is_same<T, std::string_view>::value)
        {
            return entry;
        }
        else
        {
            return std::to_string(entry);
        }
        // TODO: handle column type

    }

private:

    std::string _query;
};
}
