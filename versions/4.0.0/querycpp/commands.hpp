#pragma once

#include <string>
#include <string_view>

namespace querycpp::commands
{
    const std::string _create = "CREATE";
    const std::string _drop = "DROP";

    const std::string _select = "SELECT";
    const std::string _from = "FROM";

    const std::string _top = "TOP";

    const std::string _where = "WHERE";


    const std::string _and = "AND";
    const std::string _or = "OR";
    const std::string _in = "IN";

    const std::string _less_than = "<";
    const std::string _greater_than = ">";
    const std::string _equal = "=";

    const std::string _not = "NOT";

    const std::string _like = "LIKE";

    const std::string _right = "RIGHT";
    const std::string _left = "LEFT";

    const std::string _inner = "INNER";
    const std::string _outer = "OUTER";

    const std::string _join = "JOIN";

    const std::string _full = "FULL";
    const std::string _cross = "CROSS";
    const std::string _apply = "applt";

    const std::string _on = "ON";
    const std::string _as = "AS";

    const std::string _table = "TABLE";

    const std::string _insert = "INSERT";
    const std::string _into = "INTO";
    const std::string _values = "VALUES";


}
