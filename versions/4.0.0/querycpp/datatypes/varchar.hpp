#ifndef QUERCPP_DATATYPES_VARCHAR_HPP
#define QUERCPP_DATATYPES_VARCHAR_HPP

#include <string>
#include <cstddef>

#include <fmt/core.h>

#include <querycpp/datatypes/types.hpp>
#include <querycpp/datatypes/base_types.hpp>

namespace querycpp::datatypes
{
class varchar : public base_type
{
public:
    varchar() : _size(0) {}
    varchar(size_t size) : _size(size) {}

    std::string string() const override {

        std::string value = _size > 0 ? std::to_string(_size) : "";
        return fmt::format("{}({})", _varchar, value); // TODO: make varchar a constant
    }

private:
    size_t _size;
};
}

#endif /*QUERCPP_DATATYPES_VARCHAR_HPP*/
