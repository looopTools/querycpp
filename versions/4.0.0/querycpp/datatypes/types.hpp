#ifndef QUERCYPP_DATATYPES_TYPES_HPP
#define QUERCYPP_DATATYPES_TYPES_HPP

#include <string_view>

namespace querycpp::datatypes
{

    // Strings

    constexpr const std::string_view _character = "CHARACTER";
    constexpr const std::string_view _char = "CHAR";
    constexpr const std::string_view _bpchar = "BPCHAR";

    constexpr const std::string_view _character_varying = "CHARACTER VARYING";
    constexpr const std::string_view _varchar = "VARCHAR";

    constexpr const std::string_view _text = "TEXT";


    // Numeric
    constexpr const std::string_view _small_int = "SMALLINT";
    constexpr const std::string_view _integer = "INTEGER";
    constexpr const std::string_view _big_int = "BITINT";
    constexpr const std::string_view _decimal = "DECIMAL";
    constexpr const std::string_view _numeric = "NUMERIC";
    constexpr const std::string_view _real = "REAL";
    constexpr const std::string_view _double_precision = "DOUBLE PRECISION";
    constexpr const std::string_view _small_serial = "SMALLSERIAL";
    constexpr const std::string_view _serial = "SERIAL";
    constexpr const std::string_view _big_serial = "BIGSERIAL";

    constexpr const std::string_view _money = "MONEY";
    // Binary
    constexpr const std::string_view _bytea = "BYTEA";

}

#endif /*QUERCYPP_DATATYPES_TYPES_HPP*/
