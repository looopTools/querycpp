#ifndef QUERYCPP_DATATYPES_BASE_TYPE_HPP
#define QUERYCPP_DATATYPES_BASE_TYPE_HPP

#include <string>
#include <vector>

namespace querycpp::datatypes
{
class base_type
{
public:
    virtual std::string string() const = 0;
protected:
    std::vector<std::string> _constraints;
};
}
#endif /*QUERYCPP_DATATYPES_BASE_TYPE_HPP*/
