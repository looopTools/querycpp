#ifndef QUERYCPP_DATATYPES_SERIAL
#define QUERYCPP_DATATYPES_SERIAL

#include <querycpp/datatypes/types.hpp>

namespace querycpp::datatypes
{
class serial
{

    std::string string() const
    {
        return _serial;
    }
};
}
#endif /*QUERYCPP_DATATYPES_SERIAL*/
