#ifndef QUERCPP_DATATYPES_BYTEA_HPP
#define QUERCPP_DATATYPES_BYTEA_HPP

#include <querycpp/datatypes/types.hpp>
namespace querycpp::datatypes
{
class bytea
{
    std::string string() const {return _bytea;}
};
}

#endif /*QUERCPP_DATATYPES_BYTEA_HPP*/
