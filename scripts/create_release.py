import logging

import argparse

import pathlib
import sys
import shutil

from datetime import datetime

import re

logger = logging.getLogger(__name__)


VERSION_PATH = './versions/'

def is_semantic_version(version: str) -> bool:
    semver_pattern = re.compile(
        r'^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)' +
        r'(-([0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*))?' +
        r'(\+([0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*))?$'
    )

    # Match the pattern with the given version string
    return bool(semver_pattern.match(version))

def create_version_dir(version: str) -> None:

    version_path = pathlib.Path(f'{VERSION_PATH}{version}')

    if not version_path.parent.exists():
        logger.info(f'Creating path: {version_path.parent}')
        version_path.parent.mkdir()

    if version_path.exists():
        logger.error(f'Release: {version_path.name} already exists')
        sys.exit('Error check log file')

    version_path.mkdir(parents=True)
    logger.info(f'Creating release path: {version_path}')

def get_timestamp() -> str:

    date = datetime.now()
    return f'{date.year}{date.month}{date.day}_{date.hour}{date.minute}{date.second}'

def copy_include(version: str) -> None:

    path = pathlib.Path(f'{VERSION_PATH}{version}/querycpp')
    shutil.copytree('./include/querycpp', str(path),  dirs_exist_ok=True)
    clean_up_include(path)

def clean_up_include(dir: pathlib.Path) -> None:
    for f in dir.iterdir():
        if f.is_dir():
            clean_up_include(f)
        else:
            if f.name.endswith('~'):
                f.unlink()

def copy_txts(version: str) -> None:
    path = pathlib.Path(f'{VERSION_PATH}{version}')
    shutil.copy2("LICENSE", str(path))
    shutil.copy2("CHANGELOG.md", str(path))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='Release Script for QueryC++', description='Creates a release')
    parser.add_argument('-v', '--version', required=True, help='Semantic version for the version you want to release')
    args = parser.parse_args()

    log_file = f'{get_timestamp()}_release.log'
    logging.basicConfig(filename=log_file, level=logging.NOTSET)
    print(f'Logging to log file: {log_file}')

    if not is_semantic_version(args.version):
        logger.error(f'{args.version} is not a valid semantic version')
        sys.exit('Error check log file')

    create_version_dir(args.version)
    copy_include(args.version)
    copy_txts(args.version)
