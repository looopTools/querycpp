# Code Style

In general we follow [Googles C++ Style Guide](https://google.github.io/styleguide/cppguide.html) with a few exceptions which we describe here.

Follow the code style of the file you are in

## Naming

- All names are snaked cased
- member variables should start with `m_`

## Parentheses

- `{` and `}` should be on a their own line
  - unless a function or statement is less 20 characters in total
- Control statements must have `{` and `}` even if it is a single line body


## Spacing

- Operators must be have a leading and trailing white space.
- Control commands such as `if` and `for` must be followed by a white space
