ARG POSTGRES_VERSION=latest

FROM postgres:${POSTGRES_VERSION}

# Configure the base image
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get -y install build-essential wget zip lsb-release software-properties-common gnupg libpq-dev sqlite3 && \
    apt-get clean

# Install LLVM
ARG LLVM_VERSION=18
RUN bash -c "$(wget -O - https://apt.llvm.org/llvm.sh)" llvm.sh ${LLVM_VERSION}

WORKDIR /src
