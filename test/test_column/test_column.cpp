#include <gtest/gtest.h>

#include <querycpp/querycpp.hpp>

#include <string>

class TestDataType {

public:
    std::string string() {return "querycpp"; }
    
};

TEST(test_column, test_constructor)
{
    const std::string expected_name = "a";
    const std::string expected_type_to_string = "querycpp";
        
    auto col = querycpp::column<TestDataType>("a", TestDataType());
    EXPECT_EQ(expected_name, col.name());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
