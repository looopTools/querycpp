#include <gtest/gtest.h>

#include <querycpp/querycpp.hpp>

#include <string>

TEST(test_helpers, test_escape_string)
{
    const std::string expected = "'escaped string'";
    EXPECT_EQ(querycpp::helpers::escape_string("escaped string"), expected);
    
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
