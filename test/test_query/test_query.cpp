#include <gtest/gtest.h>

#include <querycpp/querycpp.hpp>

#include <string>

TEST(test_query, test_create_no_columns)
{
    const std::string expected = "CREATE TABLE querycpp_test";
    querycpp::query query;
    query.q_create("querycpp_test");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_create_with_columns)
{
    const std::string expected = "CREATE TABLE querycpp_test (id SERIAL, name VARCHAR(10))";
    querycpp::query query;
    query.q_create("querycpp_test", "id SERIAL", "name VARCHAR(10)");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_drop)
{
    const std::string expected = "DROP TABLE querycpp_test";
    querycpp::query query;
    query.q_drop("querycpp_test");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_insert_into_with_values_no_input) {

    const std::string expected = "INSERT INTO querycpp (id, name) VALUES";
    querycpp::query query;
    query.q_insert_into("querycpp", "id", "name").q_values();
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_insert_into_with_values_with_input) {

    const std::string expected = "INSERT INTO querycpp (id, name) VALUES (1, 'abba'), (2, 'u2')";
    querycpp::query query;
    query.q_insert_into("querycpp", "id", "name").q_values("1, 'abba'", "2, 'u2'");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_select)
{
    std::string expected = "SELECT";
    querycpp::query query;
    query.q_select();
    EXPECT_EQ(expected, query.string());

    query.clear();
    expected = "SELECT a, b, c, d";
    std::string a = "a";
    std::string b = "b";
    std::string c = "c";
    std::string d = "d";
    query.q_select(a, b, c, d);

    EXPECT_EQ(expected, query.string());

    query.clear();

    query.q_select("a", "b", "c", "d");
    EXPECT_EQ(expected, query.string());
}


TEST(test_query, test_select_all)
{
    const std::string expected = "SELECT *";
    querycpp::query query;
    query.q_select_all();
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_select_top)
{
    const std::string expected = "SELECT TOP 3";
    querycpp::query query;
    query.q_select_top(3);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_from)
{
    const std::string expected = "FROM test";
    querycpp::query query;
    query.q_from("test");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_where)
{
    const std::string expected = "WHERE";
    querycpp::query query;
    query.q_where();
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_where_col)
{
    const std::string expected = "WHERE test";
    querycpp::query query;
    query.q_where("test");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_less_than)
{
    const std::string expected = "1 < 2";
    querycpp::query query;
    query.q_less_than(1, 2);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_greater_than)
{
    const std::string expected = "1 > 2";
    querycpp::query query;
    query.q_greater_than(1, 2);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_equal)
{
    const std::string expected = "1 = 2";
    querycpp::query query;
    query.q_equal(1, 2);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_like)
{
    const std::string expected = "1 LIKE 2";
    querycpp::query query;
    query.q_like(1, 2);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_and_two_queries)
{
    const std::string expected = "name = 'querycpp' AND age > 42";
    querycpp::query query;
    query.q_equal("name", querycpp::helpers::escape_string("querycpp")).q_and().q_greater_than("age", 42);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_or_two_queries)
{
    const std::string expected = "name = 'querycpp' OR age > 42";
    querycpp::query query;
    query.q_equal("name", querycpp::helpers::escape_string("querycpp")).q_or().q_greater_than("age", 42);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test)
{
    const std::string expected = "IN col";
    querycpp::query query;
    query.q_in("col");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_in_variadic)
{
    const std::string expected = "IN (1, 2, 3, 4)";
    querycpp::query query;
    query.q_in(1, 2, 3, 4);
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_outer_join)
{
    const std::string expected = "OUTER JOIN Table2 t2";
    querycpp::query query;
    query.q_outer_join("Table2 t2");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_inner_join)
{
    const std::string expected = "INNER JOIN Table2 t2";
    querycpp::query query;
    query.q_inner_join("Table2 t2");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_right_outer_join)
{
    const std::string expected = "RIGHT OUTER JOIN Table2 t2";
    querycpp::query query;
    query.q_right_outer_join("Table2 t2");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_left_outer_join)
{
    const std::string expected = "LEFT OUTER JOIN Table2 t2";
    querycpp::query query;
    query.q_left_outer_join("Table2 t2");
    EXPECT_EQ(expected, query.string());
}

TEST(test_query, test_on)
{
    const std::string expected = "ON";
    querycpp::query query;
    query.q_on();
    EXPECT_EQ(expected, query.string());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
