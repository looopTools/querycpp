#include <querycpp/querycpp.hpp>

#include <gtest/gtest.h>

TEST(test_type, test_varchar)
{
    querycpp::datatypes::varchar x(10);
    const std::string expected = "VARCHAR(10)";
    EXPECT_EQ(x.string(), expected);

}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
