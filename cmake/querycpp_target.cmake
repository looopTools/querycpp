# Expose QueryC++ target
add_library(querycpp INTERFACE IMPORTED)

target_include_directories(querycpp INTERFACE
    $<INSTALL_INTERFACE:include>
)


install(TARGETS querycpp EXPORT querycpp)

# Export the target
install(EXPORT querycpp
    FILE querycpp_target.cmake
    DESTINATION lib/cmake/querycpp
)
