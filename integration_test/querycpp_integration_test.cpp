#include <querycpp/querycpp.hpp>
#include <pqxx/pqxx>

#include <fmt/core.h>

#include <cstdlib>
#include <string>


#include <iostream>

int main(void)
{
    pqxx::connection con;

    try
    {
        con = pqxx::connection(fmt::format("host={} port={} user={} password={} dbname={}",
                                           std::getenv("QUERY_CPP_TEST_DB_HOST"),
                                           std::getenv("QUERY_CPP_TEST_DB_PORT"),
                                           std::getenv("QUERY_CPP_TEST_DB_USER"),
                                           std::getenv("QUERY_CPP_TEST_DB_PASSWD"),
                                           std::getenv("QUERY_CPP_TEST_DB_NAME")));
    }
    catch(const std::exception& err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    const std::string table_name = "test";
    querycpp::query query;
    query.q_create(table_name, "id SERIAL", "name VARCHAR(10)");

    try
    {
        pqxx::work tx{con};
        tx.exec0(query.string());
        tx.commit();
    }
    catch(const std::exception& err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }



    query.clear();
    query.q_insert_into(table_name, "id", "name").q_values(
        fmt::format("{}, {}", 1, "'PostgreSQL'"),
        fmt::format("{}, {}", 2, "'SQLITE'"),
        fmt::format("{}, {}", 3, "'MariaDB'"));


    try
    {
        pqxx::work tx{con};
        tx.exec0(query.string());
        tx.commit();
    }
    catch (const std::exception& err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    query.clear();

    query.q_select("id", "name").q_from(table_name);

    size_t count = 0;
    try
    {
        pqxx::work tx{con};
        for (auto [id, name] : tx.stream<int, std::string_view>(query.string())) {
            std::cout << fmt::format("{}, {}, {}", count, id, name) << std::endl;
            count = count + 1;
        }
        tx.commit();
    }
    catch (const std::exception& err)
    {
        std::cerr << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    if (count != 3)
    {
        return EXIT_FAILURE;
    }

    query.clear();
    query.q_drop(table_name);

    try
    {
        pqxx::work tx{con};
        tx.exec0(query.string());
        tx.commit();
    }
    catch (const std::exception& err)
    {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    con.close();
    return EXIT_SUCCESS;
}
