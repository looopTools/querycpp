#include <querycpp/querycpp.hpp>
#include <sqlite3.h>

#include <fmt/core.h>

#include <cstdlib>


#include <string>

int main(void)
{
    const std::string db_path = "database.db";
    const std::string table_name = "test";

    querycpp::query query;

    sqlite3 *db;
    if (SQLITE_OK != sqlite3_open(db_path.c_str(), &db))
    {
        #include <cstdio>
        return EXIT_FAILURE;
    }

    query.q_create(table_name, "id NUMBER", "name VARCHAR(10)");
    {
        sqlite3_stmt *stmt;
        sqlite3_prepare(db, query.string().c_str(), -1, &stmt, NULL);
        sqlite3_step(stmt);

        if (SQLITE_OK != sqlite3_finalize(stmt))
        {
            return EXIT_FAILURE;
        }
    }

    query.clear();
    query.q_insert_into(table_name, "id", "name").q_values(
        fmt::format("{}, {}", 1, "'PostgreSQL'"),
        fmt::format("{}, {}", 2, "'SQLITE'"),
        fmt::format("{}, {}", 3, "'MariaDB'"));

    {
        sqlite3_stmt *stmt;
        sqlite3_prepare(db, query.string().c_str(), -1, &stmt, NULL);
        sqlite3_step(stmt);

        if (SQLITE_OK != sqlite3_finalize(stmt))
        {
            sqlite3_close(db);
            return EXIT_FAILURE;
        }
    }

    query.clear();
    query.q_select("id", "name").q_from(table_name);

    {
        sqlite3_stmt *stmt;
        if (SQLITE_OK != sqlite3_prepare_v2(db, query.string().c_str(), -1, &stmt, NULL))
        {
            sqlite3_close(db);
            return EXIT_FAILURE;
        }

        int return_code;
        size_t count = 0;
        while((return_code = sqlite3_step(stmt)) == SQLITE_ROW)
        {
            auto id = sqlite3_column_int(stmt, 0);
            auto name = std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1)));
            std::string msg = fmt::format("{} {} {}", count, id, name);


            std::puts(msg.c_str());
            count = count + 1;
        }

        sqlite3_finalize(stmt);

        if (count != 3)
        {
            return EXIT_FAILURE;
        }
    }

    query.clear();
    query.q_drop(table_name);

    {
        sqlite3_stmt *stmt;
        sqlite3_prepare(db, query.string().c_str(), -1, &stmt, NULL);
        if (SQLITE_OK != sqlite3_step(stmt))
        {
            sqlite3_close(db);
            return EXIT_FAILURE;
        }
    }

    if (SQLITE_OK != sqlite3_close(db))
    {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
