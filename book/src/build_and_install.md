# Build and Install

This chapter covers how to build and install QueryC++ from source.

## Requirements

For building Query C++ you will need

1. A C++ compiler supporting C++20 (we recommend clang and g++)
2. CMake minimum version 3.18
3. A copy of the source code which you can get from at [codeberg.org/ObsidianWolfLabs/querycpp](https://codeberg.org/ObsidianWolfLabs/querycpp)


# Build

From the root folder of the project run the commands below

```bash
mkdir build
cd build
cmake ..
make
```

This will do a few things.

1. Most importantly it will build QueryC++.
2. It will download all external dependencies needed to run unit tests and integration tests.
3. It will build all unit test and integration test.

# Install

In addition to the build steps in the previous build section.
Run `make install`.

# Run Unit Tests

In addition to the build steps in the previous build section.

```bash
make test
```

This will run all unit tests including those of dependencies required for unit testing and integration testing.
