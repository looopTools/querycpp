# Acknowledgement

In this chapter we acknowledge certain organisations and people.

## Special Thanks to Selected Contributors

Tibold Kandari, for helping setting up containerised integration tests.

## Time and Funding Acknowledgement


This project has preciously received support from [Aarhus University Department of Electrical and Computer Engineering, Network Computing, Communications and Storage Group](https://ece.au.dk/en/research/key-areas-in-research-and-development/communication-control-and-automation/network-computing-communications-and-storage).
These contributions lay in time allowed for a PhD student to contribute code to QueryC++.
These contributions formed the beginning of QueryC++.
