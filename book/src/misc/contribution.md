We welcome contributions to QueryC++, both code contributions, bug reports, and feature request.


# How to Contribute Code

1. Fork the repository
2. Make your changes
3. Make a Pull Request.
    - Please Rebase on main before pull request is made

All code contributions must follow the projects coding style see ['contribution/codingstyle.md'](localhost:) on Codeberg. If your contribution violates the code style, you will get a comment asking to correct this.

Please provide descriptive commit message and clean your git history as much as possible, to ensure an informative git history.
If you do not which to get pull request commits squashed on merge please say so in the pull request and inform us why.

## Working with an Issue

If you wan to work on an issue, please go to an issue on the issue tracker and ask for it to be assigned to you.
As soon as possible the issue will be assigned to you.
When it is assigned to you can start working on the issue.

## Working Without an Issue

If you want to add a new feature that has no preassigned issue.
Please reach out before you start to see if the feature is relevant for QueryC++.
We do not want you to waste time on a feature that we do not want in the library.

If the feature is deemed suitable for QueryC++, we will create an issue and assign it to you.


## Clang-Tidy

Currently we are working on providing a Clang-Tidy file that follows our code style.
But it is not ready yet.

# How to Submit a Bug Report

Please open an issue on the issue tracker and label it `Bug`.
Describe the bug as detailed as you can and provide steps to reproduce it.
Minimal reproducible working examples are great!.

If we can reproduce it we will move it to the back log and fix as soon as possible.
Otherwise we may add the `Discussion` label to the issue and continue and open discussion on how to fix the bug or help you get around it.

# How to Submit a Feature Request

Create an issue and label it `Enhancement`.
Please describe the feature your desire in as much detail as you can.


- If you feature request is simple and it is accepted, we will add to the back log.
- We will add the `Discussion` label if we find that the feature request is valid, but needs clarification.
- If we reject the feature request we will do so in a comment why on the issue and label it `won't fix`
    - You will have the option to challenge this.
    - In the case we accept your challenge we will add the label `Discussion`
    - In the case it is still rejected we will remove the `Enhancement` and `Discussion` labels and provide a comment.
    - Else we will move it to the back log
