# Usage

In this chapter we briefly discuss how to use QueryC++.

# Query class

In `<querycpp/query.hpp>` you will find the class `query`.
This is the query builder class you will use to construct your SQL queries.
All query related member functions of this class starts with `q_` to indicate they are related to queries.


# Basic Usage

To include all of QueryC++ in one go, you can include the `querycpp.hpp` file.
This file includes all other header files from QueryC++ and enables full usage QueryC++.

```c++
#include <querycpp/querycpp.hpp>

int main(void)
{
    querycpp::query query;
    query.q_select("*").q_from("t").q_where().q_greater_than('id', 10);

    auto query_str = query.string();
}
```

The above code result in the SQL statemenet `SELECT * FROM t WHERE id > 10`.
The line `auto query_str = query.string();` return the query as a string which can be parsed to a database using, for instance, [pqxx](https://github.com/jtv/libpqxx).
