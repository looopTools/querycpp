# Introduction

Welcome to the QueryC++ book.
Here you will find all the documentation you will need to use and/or contribute to QueryC++.

QueryC++ is a header only C++ library create to avoid having SQL as raw strings in your C++ code.


## License

QueryC++ is release under the BSD 3-Clause License.
For further details see [LICENSE](https://codeberg.org/ObsidianWolfLabs/querycpp/src/branch/main/LICENSE).

## Maintainer

Lars Nielsen (lars (at) obsidian (dash) wolf (dash) labs (dot) com) is the maintainer of QueryC++

## Contribution

We welcome contributions to QueryC++ from the community.
For a detailed guide on how to contribute, please see [CONTRIBUTE](https://codeberg.org/ObsidianWolfLabs/querycpp/src/branch/main/CONTRIBUTE.md).
